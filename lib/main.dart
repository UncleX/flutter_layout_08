import 'package:flutter/material.dart';

void main()=>runApp(MyApp());


class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Demo',
      home: FirstPage(),
    );
  }
}


class FirstPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Demo',
      home: Scaffold(
        appBar: AppBar(
          title: Text('第一个页面'),
        ),
        body: GestureDetector(
          onTap: (){
            print('----'+'$context');
            Navigator.push(context, MaterialPageRoute(builder: (context)=>SecondPage()));
//            Navigator.push(context, MaterialPageRoute(builder: (_) {
//              return SecondPage();
//            }));
          },
          child:Hero(
            tag: 'imageHero',
            child: Image.network(
              'https://picsum.photos/250?image=9',
              width: 100,
              height: 100,
            ),
          ),

        ),
      ),
    );
  }
}


//第二个页面
class SecondPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Demo',
      home: Scaffold(
        appBar: AppBar(
          title: Text('第二个页面'),
        ),
        body: GestureDetector(
          onTap: (){
            Navigator.pop(context);
          },
          child: Center(
            child: Hero(
              tag: 'imageHero',
              child: Image.network(
                  'https://picsum.photos/250?image=9'
              ),
            ),
          ),
        ),
      ),
    );
  }
}

/*
* MyApp一定要是StatefulWidget性质
* 涉及到网络加载图片,一定要保证手机有网
* 更改包含build的控件后,热加载是不管用的
* 直接用Image.network()添加图片,会显示的比较慢,考虑是否有懒加载的方案
* 要么先设置个占位控件,先显示出来,不至于点击都不能点击
* */